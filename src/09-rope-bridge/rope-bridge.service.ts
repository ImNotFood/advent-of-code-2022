import { Injectable } from '@nestjs/common';

export type Coordinates = [number, number];

@Injectable()
export class RopeBridgeService {
  getTotalPositionsCovered(instructionsStr: string, ropeLength: number): number {
    const rope: Coordinates[] = Array.from({ length: ropeLength }).map(() => [0, 0]);
    const visitedPositions = new Set([JSON.stringify([0, 0])]);
    const instructions = this.parseInstructions(instructionsStr);
    instructions
      .forEach(([x, y]) => {
        rope[0][0] += x;
        rope[0][1] += y;
        for (let i = 1; i < ropeLength; i++) {
          rope[i] = this.getNewTailCoordinates(rope[i - 1], rope[i]);
        }
        visitedPositions.add(JSON.stringify(rope[ropeLength - 1]));
      });
    return visitedPositions.size;
  }
  
  parseInstructions(instructions: string): Coordinates[] {
    const directionMapping: Record<string, Coordinates> = {
      L: [-1, 0],
      R: [1, 0],
      U: [0, 1],
      D: [0, -1],
    };
    return instructions
      .split('\n')
      .flatMap(instruction => {
        const [direction, length] = instruction.split(' ');
        return Array
          .from({ length: Number(length) })
          .map(() => directionMapping[direction]);
      });
  }
  
  getNewTailCoordinates(head: Coordinates, tail: Coordinates): Coordinates {
    const xDiff = head[0] - tail[0];
    const yDiff = head[1] - tail[1];
    if (Math.abs(xDiff) === 2) {
      return [tail[0] + Math.sign(xDiff), tail[1] + Math.sign(yDiff)];
    }

    if (Math.abs(yDiff) === 2) {
      return [tail[0] + Math.sign(xDiff), tail[1] + Math.sign(yDiff)];
    }

    return tail;
  }
}
