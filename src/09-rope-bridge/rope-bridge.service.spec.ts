import { Test, TestingModule } from '@nestjs/testing';
import { Coordinates, RopeBridgeService } from './rope-bridge.service';

describe('RopeBridgeService', () => {
  let service: RopeBridgeService;
  const instructions = `R 4\nU 4\nL 3\nD 1\nR 4\nD 1\nL 5\nR 2`;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RopeBridgeService],
    }).compile();

    service = module.get<RopeBridgeService>(RopeBridgeService);
  });

  describe('getNewTailCoordinates()', () => {
    const testCases: { head: Coordinates, expected: Coordinates }[] = [
      { head: [-2, -2], expected: [-1, -1] },
      { head: [-2, -1], expected: [-1, -1] },
      { head: [-2, 0], expected: [-1, 0] },
      { head: [-2, 1], expected: [-1, 1] },
      { head: [-2, 2], expected: [-1, 1] },
      { head: [-1, -2], expected: [-1, -1] },
      { head: [-1, -1], expected: [0, 0] },
      { head: [-1, 0], expected: [0, 0] },
      { head: [-1, 1], expected: [0, 0] },
      { head: [-1, 2], expected: [-1, 1] },
      { head: [0, -2], expected: [0, -1] },
      { head: [0, -1], expected: [0, 0] },
      { head: [0, 0], expected: [0, 0] },
      { head: [0, 1], expected: [0, 0] },
      { head: [0, 2], expected: [0, 1] },
      { head: [1, -2], expected: [1, -1] },
      { head: [1, -1], expected: [0, 0] },
      { head: [1, 0], expected: [0, 0] },
      { head: [1, 1], expected: [0, 0] },
      { head: [1, 2], expected: [1, 1] },
      { head: [2, -2], expected: [1, -1] },
      { head: [2, -1], expected: [1, -1] },
      { head: [2, 0], expected: [1, 0] },
      { head: [2, 1], expected: [1, 1] },
      { head: [2, 2], expected: [1, 1] },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with head${data.head} and tail[0, 0]`, () => {
        expect(service.getNewTailCoordinates(data.head, [0, 0])).toEqual(data.expected);
      });
    });
  });

  describe('parseInstructions()', () => {
    it(`should parse instructions`, () => {
      const expected = [
      [1, 0], [1, 0], [1, 0], [1, 0],
      [0, 1], [0, 1], [0, 1], [0, 1],
      [-1, 0], [-1, 0], [-1, 0],
      [0, -1],
      [1, 0], [1, 0], [1, 0], [1, 0],
      [0, -1],
      [-1, 0], [-1, 0], [-1, 0], [-1, 0], [-1, 0],
      [1, 0], [1, 0],
    ];
      expect(service.parseInstructions(instructions)).toEqual(expected);
    });
  });

  describe('getTotalPositionsCovered()', () => {
    it(`should return the correct amount`, () => {
      expect(service.getTotalPositionsCovered(instructions, 2)).toBe(13);
    });

    it(`should still work with a length of 10`, () => {
      const instructionsTen = `R 5\nU 8\nL 8\nD 3\nR 17\nD 10\nL 25\nU 20`;
      expect(service.getTotalPositionsCovered(instructionsTen, 10)).toBe(36);
    });
  });
});
