import { Body, Controller, Post } from '@nestjs/common';
import { RopeBridgeService } from './rope-bridge.service';

@Controller('rope-bridge')
export class RopeBridgeController {
  constructor(private ropeBridgeService: RopeBridgeService) {}

  @Post('unique-tail-positions')
  getUniqueTailPositions(
    @Body('instructions') instructions: string,
    @Body('ropeLength') ropeLength = 2,
  ): number {
    return this.ropeBridgeService.getTotalPositionsCovered(instructions, ropeLength);
  }
}
