import { Module } from '@nestjs/common';
import { RopeBridgeController } from './rope-bridge.controller';
import { RopeBridgeService } from './rope-bridge.service';

@Module({
  controllers: [RopeBridgeController],
  providers: [RopeBridgeService]
})
export class RopeBridgeModule {}
