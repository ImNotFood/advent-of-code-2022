import { Test, TestingModule } from '@nestjs/testing';
import { RucksackReorganizationService } from './rucksack-reorganization.service';

describe('RucksackReorganizationService', () => {
  let service: RucksackReorganizationService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RucksackReorganizationService],
    }).compile();

    service = module.get<RucksackReorganizationService>(RucksackReorganizationService);
  });

  describe('getTotalErrorPriority()', () => {
    const testCases = [
      { input: 'aAba', expected: 1 },
      { input: 'bb', expected: 2 },
      { input: 'aAbA', expected: 27 },
      { input: 'abCdzzDcBA', expected: 26 },
      { input: 'abZcdefgZh', expected: 52 },
      {
        input: 'vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw',
        expected: 157,
      },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with input = ${data.input}`, () => {
        expect(service.getTotalErrorPriority(data.input)).toBe(data.expected);
      });
    });
  });

  describe('getTotalBadgePriorities()', () => {
    const testCases = [
      { input: 'abcd\nefga\niajk', expected: 1 },
      { input: 'zbcd\nfgz\niztjk', expected: 26 },
      { input: 'bcAd\npA\nAAAAA', expected: 27 },
      { input: 'bcZd\nZ\nZZZZZZ', expected: 52 },
      {
        input: 'vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw',
        expected: 70,
      },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with input = ${data.input}`, () => {
        expect(service.getTotalBadgePriorities(data.input)).toBe(data.expected);
      });
    });
  });
});
