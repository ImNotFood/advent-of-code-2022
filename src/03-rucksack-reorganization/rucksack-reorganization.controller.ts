import { Body, Controller, Post } from '@nestjs/common';
import { RucksackReorganizationService } from './rucksack-reorganization.service';

@Controller('rucksack-reorganization')
export class RucksackReorganizationController {
  constructor(private rucksackReorganizationService: RucksackReorganizationService) {}
  
  @Post('get-total-priority')
  getTotalPriority(@Body('rucksacks') rucksacks: string): number {
    return this.rucksackReorganizationService.getTotalErrorPriority(rucksacks);
  }

  @Post('get-badge-priority')
  getBadgePriority(@Body('rucksacks') rucksacks: string): number {
    return this.rucksackReorganizationService.getTotalBadgePriorities(rucksacks);
  }
}
