import { Module } from '@nestjs/common';
import { RucksackReorganizationService } from './rucksack-reorganization.service';
import { RucksackReorganizationController } from './rucksack-reorganization.controller';

@Module({
  providers: [RucksackReorganizationService],
  controllers: [RucksackReorganizationController]
})
export class RucksackReorganizationModule {}
