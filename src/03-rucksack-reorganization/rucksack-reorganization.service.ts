import { Injectable } from '@nestjs/common';

@Injectable()
export class RucksackReorganizationService {
  // I KNOW THIS IS STUPID
  // The use case isn't ideal but I really wanted to write a fluent API
  // While keeping it typed
  private stringBackpacks = '';
  private arrayBackpacks: string[] = [];
  private compartimentedBackpacks: [string, string][] = [];
  private wrongItems: string[] = [];

  getTotalErrorPriority(backpacks: string): number {
    this.stringBackpacks = backpacks;
    const itemPriorities = this
      .separateBackpacks()
      .splitBackpackCompartments()
      .filterWrongItems()
      .mapItemsToPriorities(this.wrongItems);
    return this.getTotalPriorities(itemPriorities);
  }

  private separateBackpacks(): RucksackReorganizationService {
    this.arrayBackpacks = this.stringBackpacks.split('\n');
    return this;
  }

  private splitBackpackCompartments(): RucksackReorganizationService {
    this.compartimentedBackpacks = this
      .arrayBackpacks
      .map(backpack => {
        const middle = Math.ceil(backpack.length / 2);
        return [
          backpack.slice(0, middle),
          backpack.slice(middle)
        ]}
      );
    return this;
  }

  private filterWrongItems(): RucksackReorganizationService {
    // @ts-ignore
    this.wrongItems = this
      .compartimentedBackpacks
      .map(backpack => {
        const [left, right] = backpack;
        for (let item of left) {
          if (right.includes(item)) {
            return item;
          }
        }
      });
    return this;
  }

  private mapItemsToPriorities(itemList: string[]): number[] {
    return itemList.map(item => this.mapCharacterToPriority(item));
  }

  private mapCharacterToPriority(character: string): number {
    const characterCode = character.charCodeAt(0);
    if (characterCode >= 97) { // 'a'
      return characterCode - 96;
    }
    return characterCode - 38;
  }

  private getTotalPriorities(priorities: number[]): number {
    return priorities.reduce((total, current) => total + current, 0);
  }

  private backpacksGroupedByThree: [string, string, string][] = [];
  private badges: string[] = [];

  getTotalBadgePriorities(backpacks: string): number {
    this.stringBackpacks = backpacks;
    const itemPriorities = this
      .separateBackpacks()
      .groupBackpacksByThree()
      .findCommonBadges()
      .mapItemsToPriorities(this.badges);
    return this.getTotalPriorities(itemPriorities);
  }

  private groupBackpacksByThree(): RucksackReorganizationService {
    for (let i = 0; i < this.arrayBackpacks.length; i += 3) {
      this.backpacksGroupedByThree[i / 3] = [
        this.arrayBackpacks[i], this.arrayBackpacks[i + 1], this.arrayBackpacks[i + 2]
      ];
    }
    return this;
  }

  private findCommonBadges(): RucksackReorganizationService {
    // @ts-ignore
    this.badges = this.backpacksGroupedByThree.map(backpackGroup => {
      const sortedBackpacks = backpackGroup.sort((a, b) => a.length - b.length);
      for (let item of sortedBackpacks[0]) {
        if (sortedBackpacks[1].includes(item)) {
          if (sortedBackpacks[2].includes(item)) {
            return item;
          }
        }
      }
    });
    return this;
  }
}
