import { Command, DirectoryTree } from './models';
import { NoSpaceLeftParser } from './no-space-left.parser';

describe('NoSpaceLeftParser', () => {
  const rawCommands = `$ cd /\n$ ls\ndir a\n14848514 b.txt\n8504156 c.dat\ndir d\n$ cd a\n$ ls\ndir e\n29116 f\n2557 g\n62596 h.lst\n$ cd e\n$ ls\n584 i\n$ cd ..\n$ cd ..\n$ cd d\n$ ls\n4060174 j\n8033020 d.log\n5626152 d.ext\n7214296 k`;
  const parsedCommands: Command[] = [
    new Command({ command: 'cd', args: '/' }),
    new Command({ command: 'ls', result: ['dir a', '14848514 b.txt', '8504156 c.dat', 'dir d' ] }),
    new Command({ command: 'cd', args: 'a' }),
    new Command({ command: 'ls', result: ['dir e', '29116 f', '2557 g', '62596 h.lst'] }),
    new Command({ command: 'cd', args: 'e' }),
    new Command({ command: 'ls', result: ['584 i'] }),
    new Command({ command: 'cd', args: '..' }),
    new Command({ command: 'cd', args: '..' }),
    new Command({ command: 'cd', args: 'd' }),
    new Command({ command: 'ls', result: ['4060174 j', '8033020 d.log', '5626152 d.ext', '7214296 k'] }),
  ];

  const tree = new DirectoryTree({
    '/': new DirectoryTree({
      a: new DirectoryTree({
        e: new DirectoryTree({
          i: 584,
        }),
        f: 29116,
        g: 2557,
        'h.lst': 62596,
      }),
      'b.txt': 14848514,
      'c.dat': 8504156,
      d: new DirectoryTree({
        j: 4060174,
        'd.log': 8033020,
        'd.ext': 5626152,
        k: 7214296,
      }),
    }),
  });
  
  it(`should parse commands`, () => {
    expect(NoSpaceLeftParser.parseCommands(rawCommands)).toEqual(parsedCommands);
  });
  
  it(`should build directory tree`, () => {
    expect(NoSpaceLeftParser.buildDirectoryTree(parsedCommands)).toEqual(tree);
  });
});