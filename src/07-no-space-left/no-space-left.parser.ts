import { Command, DirectoryTree } from './models';

export class NoSpaceLeftParser {
  static parseCommands(commands: string): Command[] {
    return commands
      .split('$ ')
      .filter(c => c)
      .map(NoSpaceLeftParser.parseCommand);
  }

  private static parseCommand(command: string): Command {
    const arrayCommand = command
      .split('\n')
      .filter(c => c);
    
    if (arrayCommand[0].startsWith('cd ')) {
      const [commandName, args] = arrayCommand[0].split(' ');
      return new Command({ command: commandName, args });
    }

    const [commandName, ...result] = arrayCommand;
    return new Command({
      command: commandName,
      result: result,
    });
  }

  static buildDirectoryTree(commands: Command[]): DirectoryTree {
    const currentPath: string[] = [];
    const tree = new DirectoryTree();
    
    commands.forEach(c => {
      const { command, args, result } = c;

      if (command === 'cd') {
        if (args === '..') {
          currentPath.pop();
          return;
        }
        currentPath.push(args!);
        tree.setProperty(new DirectoryTree(), currentPath);
      }

      if (command === 'ls') {
        result?.forEach(file => {
          if (/^\d/.test(file)) {
            const [size, fileName] = file.split(' ');
            tree.setProperty(Number(size), currentPath.concat(fileName));
          }
        });
      }
    });

    return tree;
  }
}