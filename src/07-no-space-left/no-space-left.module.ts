import { Module } from '@nestjs/common';
import { NoSpaceLeftService } from './no-space-left.service';
import { NoSpaceLeftController } from './no-space-left.controller';

@Module({
  providers: [NoSpaceLeftService],
  controllers: [NoSpaceLeftController]
})
export class NoSpaceLeftModule {}
