export class Command {
  command: string;
  args?: string;
  result?: string[];

  constructor(command: Command) {
    this.command = command.command;
    this.args = command.args;
    this.result = command.result;
  }
}

export class DirectoryTree {
  [key: string]: DirectoryTree | number | Function;

  constructor(tree?: Partial<DirectoryTree>) {
    if (!tree) return;

    Object
      .keys(tree)
      .forEach(key => this[key] = tree[key]!);
  }

  setProperty(value: DirectoryTree | number, path: string[]): void {
    if (path.length === 1) {
      this[path[0]] = value;
      return;
    }
    (this[path[0]] as DirectoryTree).setProperty(value, path.slice(1));
  }

  getProperty(path: string[]): DirectoryTree | number {
    if (path.length === 1) {
      return this[path[0]] as DirectoryTree | number;
    }
    return this.getProperty(path.slice(1));
  }
}