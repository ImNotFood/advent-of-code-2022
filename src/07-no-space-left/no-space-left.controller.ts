import { Body, Controller, Post } from '@nestjs/common';
import { NoSpaceLeftService } from './no-space-left.service';

@Controller('no-space-left')
export class NoSpaceLeftController {
  constructor(private noSpaceLeftService: NoSpaceLeftService) {}

  @Post('small-max-size')
  getSmallMaxSize(@Body('commands') commands: string) {
    return this.noSpaceLeftService.getSmallDirectoriesTotalSize(commands);
  }

  @Post('size-to-delete')
  getSizeToDelete(@Body('commands') commands: string) {
    return this.noSpaceLeftService.getSmallestSizeToDelete(commands);
  }
}
