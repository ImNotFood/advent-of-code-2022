import { Injectable } from '@nestjs/common';
import { DirectoryTree } from './models';
import { NoSpaceLeftParser } from './no-space-left.parser';

@Injectable()
export class NoSpaceLeftService {
  getSmallDirectoriesTotalSize(rawCommands: string): number {
    const commands = NoSpaceLeftParser.parseCommands(rawCommands);
    const tree = NoSpaceLeftParser.buildDirectoryTree(commands);
    return this.getSumOfDirectoriesSmallerThan(100000, tree);
  }

  private getSumOfDirectoriesSmallerThan(maxSize: number, tree: DirectoryTree): number {    
    return Object
      .values(tree)
      .filter((node): node is DirectoryTree => node instanceof DirectoryTree)
      .reduce<number>((total, directory) => {
        const dirSize = this.getDirectorySize(directory);
        if (dirSize <= maxSize) {
          total += dirSize;
        }
        return total += this.getSumOfDirectoriesSmallerThan(maxSize, directory);
      }, 0);
  }

  getDirectorySize(directory: DirectoryTree): number {
    return Object
      .keys(directory)
      .reduce<number>((total, fileName) => {
        const file = directory[fileName];
        return typeof file === 'number' ?
          total += file :
          total += this.getDirectorySize(file as DirectoryTree);
      }, 0);
  }

  getSmallestSizeToDelete(rawCommands: string): number {
    const commands = NoSpaceLeftParser.parseCommands(rawCommands);
    const rootTree = NoSpaceLeftParser.buildDirectoryTree(commands);
    const maxSize = 70000000;
    const requiredFreeSpace = 30000000;
    const usedSpace = this.getDirectorySize(rootTree);
    const currentlyFreeSpace = maxSize - usedSpace;
    const spaceToFree = requiredFreeSpace - currentlyFreeSpace;
    return this.getMinSizeOfAllChildren(rootTree, spaceToFree, maxSize);
  }

  private getMinSizeOfAllChildren(rootDirectory: DirectoryTree, spaceToFree: number, currentMinSize: number): number {
    return Object
      .values(rootDirectory)
      .filter((node): node is DirectoryTree => node instanceof DirectoryTree)
      .reduce<number>((minSize, directory) => {
        const directorySize = this.getDirectorySize(directory);
        return Math.min(this.getMinSize(spaceToFree, directorySize, minSize), this.getMinSizeOfAllChildren(directory, spaceToFree, minSize));
      }, currentMinSize);
  }

  private getMinSize(spaceToFree: number, directorySize: number, currentMinSize: number): number {
    if (directorySize >= spaceToFree) {
      return Math.min(currentMinSize, directorySize);
    }
    return currentMinSize;
  }
}
