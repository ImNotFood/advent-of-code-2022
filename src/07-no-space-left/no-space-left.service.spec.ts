import { Test, TestingModule } from '@nestjs/testing';
import { DirectoryTree } from './models';
import { NoSpaceLeftService } from './no-space-left.service';

describe('NoSpaceLeftService', () => {
  let service: NoSpaceLeftService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [NoSpaceLeftService],
    }).compile();

    service = module.get<NoSpaceLeftService>(NoSpaceLeftService);
  });

  const rawCommands = `$ cd /\n$ ls\ndir a\n14848514 b.txt\n8504156 c.dat\ndir d\n$ cd a\n$ ls\ndir e\n29116 f\n2557 g\n62596 h.lst\n$ cd e\n$ ls\n584 i\n$ cd ..\n$ cd ..\n$ cd d\n$ ls\n4060174 j\n8033020 d.log\n5626152 d.ext\n7214296 k`;

  it(`should return the correct size`, () => {
    expect(service.getSmallDirectoriesTotalSize(rawCommands)).toBe(95437);
  });

  const tree = new DirectoryTree({
    '/': new DirectoryTree({
      a: new DirectoryTree({
        e: new DirectoryTree({
          i: 584,
        }),
        f: 29116,
        g: 2557,
        'h.lst': 62596,
      }),
      'b.txt': 14848514,
      'c.dat': 8504156,
      d: new DirectoryTree({
        j: 4060174,
        'd.log': 8033020,
        'd.ext': 5626152,
        k: 7214296,
      }),
    }),
  });

  it(`should return directory size`, () => {
    expect(service.getDirectorySize(tree)).toBe(48381165);
  });

  it(`should return smallest size to delete`, () => {
    expect(service.getSmallestSizeToDelete(rawCommands)).toBe(24933642);
  });
});
