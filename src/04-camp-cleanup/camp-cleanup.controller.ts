import { Body, Controller, Post } from '@nestjs/common';
import { CampCleanupService } from './camp-cleanup.service';

@Controller('camp-cleanup')
export class CampCleanupController {
  constructor(private campCleanupService: CampCleanupService) {}

  @Post('get-full-overlaps')
  getFullOverlaps(@Body('assignments') assignments: string) {
    return this.campCleanupService.getFullyOverlappedAssignments(assignments);
  }

  @Post('get-partial-overlaps')
  getPartialOverlaps(@Body('assignments') assignments: string) {
    return this.campCleanupService.getPartiallyOverlappedAssignments(assignments);
  }
}
