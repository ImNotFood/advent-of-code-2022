import { Test, TestingModule } from '@nestjs/testing';
import { CampCleanupService } from './camp-cleanup.service';

describe('CampCleanupService', () => {
  let service: CampCleanupService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CampCleanupService],
    }).compile();

    service = module.get<CampCleanupService>(CampCleanupService);
  });

  describe('getTotalOverlappedAssignments()', () => {
    const testCases = [
      { input: '1-4,1-4', expected: 1 },
      { input: '1-4,1-5', expected: 1 },
      { input: '1-5,1-4', expected: 1 },
      { input: '1-4,2-5', expected: 0 },
      { input: '2-5,1-4', expected: 0 },
      { input: '1-4,2-3', expected: 1 },
      { input: '2-3,1-4', expected: 1 },
      { input: '10-120,11-121', expected: 0 },
      { input: '11-121,10-120', expected: 0 },
      { input: '10-120,100-101', expected: 1 },
      { input: '100-101,10-120', expected: 1 },
      { input: '2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8', expected: 2 },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with input = ${data.input}`, () => {
        expect(service.getFullyOverlappedAssignments(data.input)).toBe(data.expected);
      });
    });
  });

  describe('getPartiallyOverlappedAssignments()', () => {
    const testCases = [
      { input: '1-4,1-4', expected: 1 },
      { input: '1-4,1-5', expected: 1 },
      { input: '1-5,1-4', expected: 1 },
      { input: '1-4,2-5', expected: 1 },
      { input: '2-5,1-4', expected: 1 },
      { input: '1-4,2-3', expected: 1 },
      { input: '2-3,1-4', expected: 1 },
      { input: '1-5,6-8', expected: 0 },
      { input: '6-8,1-5', expected: 0 },
      { input: '5-7,7-9', expected: 1 },
      { input: '1-1,2-2', expected: 0 },
      { input: '10-120,11-121', expected: 1 },
      { input: '11-121,10-120', expected: 1 },
      { input: '10-120,100-101', expected: 1 },
      { input: '100-101,10-120', expected: 1 },
      { input: '2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8', expected: 4 },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with input = ${data.input}`, () => {
        expect(service.getPartiallyOverlappedAssignments(data.input)).toBe(data.expected);
      });
    })
  });
});
