import { Module } from '@nestjs/common';
import { CampCleanupService } from './camp-cleanup.service';
import { CampCleanupController } from './camp-cleanup.controller';

@Module({
  providers: [CampCleanupService],
  controllers: [CampCleanupController]
})
export class CampCleanupModule {}
