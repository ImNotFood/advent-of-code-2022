import { assignMetadata, Injectable } from '@nestjs/common';

interface Assignment { start: number; end: number }
type AssignmentPair = Assignment[];

@Injectable()
export class CampCleanupService {
  getFullyOverlappedAssignments(rawAssignments: string): number {
    return this
      .parseAssignments(rawAssignments)
      .reduce((total, assignment) => total += this.areAssignmentsFullyOverlapping(assignment), 0);
  }

  private parseAssignments(rawAssignments: string): Array<AssignmentPair> {
    return rawAssignments // "1-2,2-3\n3-4,4-5"
      .split('\n') // ["1-2,2-3", "3-4,4-5"]
      .map(assignmentPair => assignmentPair
        .split(',') // [["1-2", "2-3"], ["3-4"], ["4-5"]]
        .map(assignment => ({
            start: parseInt(assignment.split('-')[0]),
            end: parseInt(assignment.split('-')[1]),
          })
        )
      );
  }

  private areAssignmentsFullyOverlapping(assignments: AssignmentPair): 0 | 1 {
    const [left, right] = assignments;
    if (left.start >= right.start && left.end <= right.end) {
      return 1;
    } else if (left.start <= right.start && left.end >= right.end) {
      return 1;
    }
    return 0;
  }

  getPartiallyOverlappedAssignments(rawAssignments: string): number {
    return this
      .parseAssignments(rawAssignments)
      .reduce((total, assignment) => total += this.areAssignmentPartiallyOverlapping(assignment), 0);
  }

  private areAssignmentPartiallyOverlapping(assignments: AssignmentPair): 0 | 1 {
    const [left, right] = assignments;
    return this.isNumberContained(right.start, left.start, left.end) ||
      this.isNumberContained(right.end, left.start, left.end) ||
      this.isNumberContained(left.start, right.start, right.end) ||
      this.isNumberContained(left.end, right.start, right.end);
  }

  private isNumberContained(value: number, start: number, end: number): 0 | 1 {
    return start <= value && end >= value ? 1 : 0;
  }
}
