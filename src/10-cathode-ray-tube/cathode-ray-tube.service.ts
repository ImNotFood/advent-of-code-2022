import { Injectable } from '@nestjs/common';
import { filter, from, map, mergeScan, Observable, of, pairwise, reduce, scan, startWith, tap } from 'rxjs';

class Cycle {
  index: number;
  value: number;

  constructor(cycle?: Cycle) {
    this.index = cycle?.index ?? 0;
    this.value = cycle?.value ?? 1;
  }
}

type Instruction = 'addx' | 'noop';

@Injectable()
export class CathodeRayTubeService {
  getSumFromCycles(instructionsStr: string, cyclesToWatch: number[]): Observable<number> {
    const sortedCycles = cyclesToWatch.sort((a, b) => a - b);
    const instructions = instructionsStr
      .split('\n')
      .map(instruction => instruction.split(' '));

    return from(instructions).pipe(
      mergeScan((currentCycle, [instruction, arg]) => this[instruction as Instruction](currentCycle, arg), new Cycle()),
      filter(cycle => sortedCycles[0] === cycle.index),
      tap(() => sortedCycles.shift()),
      reduce<Cycle, number>((total, { index, value }) => total += index * value, 0),
    );
  }

  private addx(currentCycle: Cycle, valueToAdd: string): Observable<Cycle> {
    const { index, value } = currentCycle;
    return from([
        new Cycle({ index: index + 1, value }),
        new Cycle({ index: index + 2, value }),
        new Cycle({ index: index + 2, value: value + Number(valueToAdd),
      }),
    ]);
  }

  private noop(currentCycle: Cycle): Observable<Cycle> {
    return of(new Cycle({
      index: currentCycle.index + 1,
      value: currentCycle.value,
    }));
  }

  draw(instructionsStr: string): Observable<string> {
    const instructions = instructionsStr
      .split('\n')
      .map(instruction => instruction.split(' '));

    return from(instructions).pipe(
      mergeScan((currentCycle, [instruction, arg]) => this[instruction as Instruction](currentCycle, arg), new Cycle()),
      reduce<Cycle, string>((screen, { index, value }) => {
        if (index === screen.length) {
          return screen;
        }
        if ([value - 1, value, value + 1].includes(screen.length % 40)) {
          return screen + '#';
        }
        return screen + '.';
      }, ''),
      map(screen => screen.match(/.{1,40}/g)!.join('\n')),
    );
  }
}
