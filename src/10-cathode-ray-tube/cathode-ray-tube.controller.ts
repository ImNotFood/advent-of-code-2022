import { Body, Controller, Post } from '@nestjs/common';
import { Observable } from 'rxjs';
import { CathodeRayTubeService } from './cathode-ray-tube.service';

@Controller('cathode-ray-tube')
export class CathodeRayTubeController {
  constructor(private cathodeRayTubeService: CathodeRayTubeService) {}

  @Post('every-40-cycle')
  getTotalEvery40Cycle(
    @Body('instructions') instructions: string,
    @Body('cycles') cycles: number[],
  ): Observable<number> {
    return this.cathodeRayTubeService.getSumFromCycles(instructions, cycles);
  }

  @Post('displayed-characters')
  getDisplayedCharacters(
    @Body('instructions') instructions: string
  ): Observable<string> {
    return this.cathodeRayTubeService.draw(instructions);
  }
}
