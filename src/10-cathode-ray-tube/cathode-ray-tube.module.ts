import { Module } from '@nestjs/common';
import { CathodeRayTubeController } from './cathode-ray-tube.controller';
import { CathodeRayTubeService } from './cathode-ray-tube.service';

@Module({
  controllers: [CathodeRayTubeController],
  providers: [CathodeRayTubeService]
})
export class CathodeRayTubeModule {}
