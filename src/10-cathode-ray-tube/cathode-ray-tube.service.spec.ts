import { Test, TestingModule } from '@nestjs/testing';
import { CathodeRayTubeService } from './cathode-ray-tube.service';

describe('CathodeRayTubeService', () => {
  let service: CathodeRayTubeService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CathodeRayTubeService],
    }).compile();

    service = module.get<CathodeRayTubeService>(CathodeRayTubeService);
  });

  it(`should calculate total from small input`, done => {
    const instructions = `noop\naddx 3\naddx -5`;
    service
      .getSumFromCycles(instructions, [1, 2, 3, 4, 5])
      .subscribe(total => {
        expect(total).toBe(42);
        done();
      });
  });

  it(`should work`, done => {
    const instructions = `noop\naddx 3\naddx -4\nnoop`;
    service
      .getSumFromCycles(instructions, [])
      .subscribe(total => {
        expect(total).toBe(0);
        done();
      })
  });

  const exampleInstructions = `addx 15\naddx -11\naddx 6\naddx -3\naddx 5\naddx -1\naddx -8\naddx 13\naddx 4\nnoop\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx -35\naddx 1\naddx 24\naddx -19\naddx 1\naddx 16\naddx -11\nnoop\nnoop\naddx 21\naddx -15\nnoop\nnoop\naddx -3\naddx 9\naddx 1\naddx -3\naddx 8\naddx 1\naddx 5\nnoop\nnoop\nnoop\nnoop\nnoop\naddx -36\nnoop\naddx 1\naddx 7\nnoop\nnoop\nnoop\naddx 2\naddx 6\nnoop\nnoop\nnoop\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 7\naddx 1\nnoop\naddx -13\naddx 13\naddx 7\nnoop\naddx 1\naddx -33\nnoop\nnoop\nnoop\naddx 2\nnoop\nnoop\nnoop\naddx 8\nnoop\naddx -1\naddx 2\naddx 1\nnoop\naddx 17\naddx -9\naddx 1\naddx 1\naddx -3\naddx 11\nnoop\nnoop\naddx 1\nnoop\naddx 1\nnoop\nnoop\naddx -13\naddx -19\naddx 1\naddx 3\naddx 26\naddx -30\naddx 12\naddx -1\naddx 3\naddx 1\nnoop\nnoop\nnoop\naddx -9\naddx 18\naddx 1\naddx 2\nnoop\nnoop\naddx 9\nnoop\nnoop\nnoop\naddx -1\naddx 2\naddx -37\naddx 1\naddx 3\nnoop\naddx 15\naddx -21\naddx 22\naddx -6\naddx 1\nnoop\naddx 2\naddx 1\nnoop\naddx -10\nnoop\nnoop\naddx 20\naddx 1\naddx 2\naddx 2\naddx -6\naddx -11\nnoop\nnoop\nnoop`;

  it(`should calculate total for given cycles`, done => {
    service
      .getSumFromCycles(exampleInstructions, [20, 60, 100, 140, 180, 220])
      .subscribe(total => {
        expect(total).toBe(13140);
        done();
      });
  });

  it(`should draw properly`, done => {
    const expectedScreen = `##..##..##..##..##..##..##..##..##..##..\n###...###...###...###...###...###...###.\n####....####....####....####....####....\n#####.....#####.....#####.....#####.....\n######......######......######......####\n#######.......#######.......#######.....`;
    service
      .draw(exampleInstructions)
      .subscribe((screen: any) => {
        try {
          expect(screen).toBe(expectedScreen);
          done();
        } catch (err) {
          done(err);
        }
      });
  });
});
