import { Module } from '@nestjs/common';
import { CalorieCountingModule } from './01-calorie-counting/calorie-counting.module';
import { RockPaperScissorsModule } from './02-rock-paper-scissors/rock-paper-scissors.module';
import { RucksackReorganizationModule } from './03-rucksack-reorganization/rucksack-reorganization.module';
import { CampCleanupModule } from './04-camp-cleanup/camp-cleanup.module';
import { SupplyStacksModule } from './05-supply-stacks/supply-stacks.module';
import { TuningTroubleModule } from './06-tuning-trouble/tuning-trouble.module';
import { NoSpaceLeftModule } from './07-no-space-left/no-space-left.module';
import { TreetopTreeHouseModule } from './08-treetop-tree-house/treetop-tree-house.module';
import { RopeBridgeModule } from './09-rope-bridge/rope-bridge.module';
import { CathodeRayTubeModule } from './10-cathode-ray-tube/cathode-ray-tube.module';

@Module({
  imports: [CalorieCountingModule, RockPaperScissorsModule, RucksackReorganizationModule, CampCleanupModule, SupplyStacksModule, TuningTroubleModule, NoSpaceLeftModule, TreetopTreeHouseModule, RopeBridgeModule, CathodeRayTubeModule],
})
export class AppModule {}
