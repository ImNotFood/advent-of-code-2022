import { Body, Controller, Post } from '@nestjs/common';
import { TuningTroubleService } from './tuning-trouble.service';

@Controller('tuning-trouble')
export class TuningTroubleController {
  constructor(private tuningTroubleService: TuningTroubleService) {}

  @Post('get-first-marker')
  getFirstMarker(@Body('buffer') buffer: string): number {
    return this.tuningTroubleService.getFirstMarkerIndex(buffer)!;
  }

  @Post('get-first-message')
  getFirstMessage(@Body('buffer') buffer: string): number {
    return this.tuningTroubleService.getFirstMessageIndex(buffer)!;
  }
}
