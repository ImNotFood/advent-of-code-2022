import { Injectable } from '@nestjs/common';

@Injectable()
export class TuningTroubleService {
  getFirstMarkerIndex(buffer: string): number | undefined {
    for (let i = 3; i < buffer.length; i++) {
      if (new Set(buffer.slice(i - 3, i + 1)).size >= 4) {
        return i + 1;
      }
    }
  }

  getFirstMessageIndex(buffer: string): number | undefined {
    for (let i = 13; i < buffer.length; i++) {
      if (new Set(buffer.slice(i - 13, i + 1)).size >= 14) {
        return i + 1;
      }
    }
  }
}
