import { Test, TestingModule } from '@nestjs/testing';
import { TuningTroubleService } from './tuning-trouble.service';

describe('TuningTroubleService', () => {
  let service: TuningTroubleService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TuningTroubleService],
    }).compile();

    service = module.get<TuningTroubleService>(TuningTroubleService);
  });

  describe('getFirstMarkerIndex()', () => {
    const testCases = [
      { input: 'bvwbjplbgvbhsrlpgdmjqwftvncz', expected: 5 },
      { input: 'nppdvjthqldpwncqszvftbrmjlhg', expected: 6 },
      { input: 'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', expected: 10 },
      { input: 'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', expected: 11 },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with input = ${data.input}`, () => {
        expect(service.getFirstMarkerIndex(data.input)).toBe(data.expected);
      });
    });
  });

  describe('getFirstMessageIndex()', () => {
    const testCases = [
      { input: 'mjqjpqmgbljsphdztnvjfqwrcgsmlb', expected: 19 },
      { input: 'bvwbjplbgvbhsrlpgdmjqwftvncz', expected: 23 },
      { input: 'nppdvjthqldpwncqszvftbrmjlhg', expected: 23 },
      { input: 'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg', expected: 29 },
      { input: 'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw', expected: 26 },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with input = ${data.input}`, () => {
        expect(service.getFirstMessageIndex(data.input)).toBe(data.expected);
      });
    });
  });
});
