import { Module } from '@nestjs/common';
import { TuningTroubleController } from './tuning-trouble.controller';
import { TuningTroubleService } from './tuning-trouble.service';

@Module({
  controllers: [TuningTroubleController],
  providers: [TuningTroubleService]
})
export class TuningTroubleModule {}
