import { Injectable } from '@nestjs/common';
import { SupplyStacksParser } from './supply-stack-parser';

@Injectable()
export class SupplyStacksService {
  moveCratesOneByOne(plan: string): string {
    const { crates, instructions } = this.parsePlan(plan);

    instructions.forEach(instruction => {
      const [amount, start, end] = instruction.map(i => i - 1);

      for (let i = 0; i <= amount; i++) {
        crates[end].push(crates[start].pop()!);
      }
    });
    return this.getTopCrates(crates);
  }

  moveCratesInBulk(plan: string): string {
    const { crates, instructions } = this.parsePlan(plan);

    instructions.forEach(instruction => {
      const [amount, start, end] = instruction.map(i => i - 1);
      const removedCrates = crates[start].splice(crates[start].length - amount - 1);
      crates[end] = crates[end].concat(removedCrates);
    });
    return this.getTopCrates(crates);
  }

  private parsePlan(plan: string): { crates: string[][], instructions: number[][] } {
    const [cratesStr, instructionsStr] = plan.split('\n\n');
    return {
      crates: SupplyStacksParser.parseCrates(cratesStr),
      instructions: SupplyStacksParser.parseInstructions(instructionsStr),
    };
  }

  private getTopCrates(crates: string[][]): string {
    return crates
      .flatMap(stack => stack.at(-1))
      .join('');
  }
}
