import { SupplyStacksParser } from './supply-stack-parser';

describe('SupplyStackParser', () => {
  it(`should parse crates properly`, () => {
    const crates = `    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 `;
    expect(SupplyStacksParser.parseCrates(crates)).toEqual([['Z', 'N'], ['M', 'C', 'D'], ['P']]);
  });

  it(`should parse instructions properly`, () => {
    const instructions = `move 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2`;
    expect(SupplyStacksParser.parseInstructions(instructions)).toEqual([[1, 2, 1], [3, 1, 3], [2, 2, 1], [1, 1, 2]]);
  });
});