import { Module } from '@nestjs/common';
import { SupplyStacksService } from './supply-stacks.service';
import { SupplyStacksController } from './supply-stacks.controller';

@Module({
  providers: [SupplyStacksService],
  controllers: [SupplyStacksController]
})
export class SupplyStacksModule {}
