import { Body, Controller, Post } from '@nestjs/common';
import { SupplyStacksService } from './supply-stacks.service';

@Controller('supply-stacks')
export class SupplyStacksController {
  constructor(private supplyStacksService: SupplyStacksService) {}

  @Post('top-crates')
  getTopCrates(@Body('plan') plan: string): string {
    return this.supplyStacksService.moveCratesOneByOne(plan);
  }

  @Post('top-crates-bulk')
  getTopCratesBulk(@Body('plan') plan: string): string {
    return this.supplyStacksService.moveCratesInBulk(plan);
  }
}
