export class SupplyStacksParser {
  static parseCrates(crateStacks: string): string[][] {
    const stacks = crateStacks.split('\n').reverse().slice(1); // [[]]
    const height = stacks.length;
    const cratesPerRow = stacks[0].length;
    const result: string[][] = [];
    
    for (let column = 1; column < cratesPerRow; column += 4) {
      const columnIndex = (column - 1) / 4;
      result.push([]);

      for (let row = 0; row < height; row++) {
        const crate = stacks[row][column].trim();
        if (crate) {
          result[columnIndex].push(crate);
        } else {
          row = cratesPerRow;
        }
      }
    }

    return result;
  }

  static parseInstructions(instructions: string): number[][] {
    return instructions
      .split('\n')
      .map(instruction => instruction.replaceAll(/[^\d]+/g, ' ').trim())
      .map(instruction => instruction.split(' '))
      .map(array => array.map(i => parseInt(i)));
  }
}