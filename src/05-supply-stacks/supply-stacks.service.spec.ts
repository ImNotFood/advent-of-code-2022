import { Test, TestingModule } from '@nestjs/testing';
import { SupplyStacksService } from './supply-stacks.service';

describe('SupplyStacksService', () => {
  let service: SupplyStacksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SupplyStacksService],
    }).compile();

    service = module.get<SupplyStacksService>(SupplyStacksService);
  });

  it(`should move crates one by one`, () => {
    const plan = `    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2`;
    expect(service.moveCratesOneByOne(plan)).toBe('CMZ');
  });

  it(`should move crates by bulk`, () => {
    const plan = `    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2`;
    expect(service.moveCratesInBulk(plan)).toBe('MCD');
  });
});
