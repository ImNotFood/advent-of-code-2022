import { Injectable, Scope } from '@nestjs/common';

@Injectable({ scope: Scope.REQUEST })
export class TreetopTreeHouseService {
  private forest: string[] = [];

  countVisibleTrees(forestStr: string): number {
    this.forest = forestStr.split('\n');
    return this
      .findVisibleTrees()
      .flat()
      .reduce((total, visible) => total + visible, 0);
  }

  getBestScenicScore(forestStr: string): number {
    this.forest = forestStr.split('\n');
    return Math.max(...this
      .getScenicScoreMask()
      .flat()
    );
  }

  private findVisibleTrees(): number[][] {
    const visibilityMask = this.createMaskFromForest();

    for (let row = 0; row < this.columnLength; row++) {
      let smallestTrees = { bottom: -1, left: -1, right: -1, top: -1 };
      for (let col = 0; col < this.rowLength; col++) {
        const coordinateValue = row * this.rowLength + col;
        const rowVertical = coordinateValue % this.columnLength;
        const colVertical = Math.floor(coordinateValue / this.columnLength);
        smallestTrees = {
          bottom: this.isTreeVisible(this.columnLength - rowVertical - 1, colVertical, smallestTrees.bottom, visibilityMask),
          left: this.isTreeVisible(row, col, smallestTrees.left, visibilityMask),
          right: this.isTreeVisible(row, this.rowLength - col - 1, smallestTrees.right, visibilityMask),
          top: this.isTreeVisible(rowVertical, colVertical, smallestTrees.top, visibilityMask),
        };
      }
    }
    return visibilityMask;
  }

  private isTreeVisible(row: number, col: number, smallestTree: number, visibilityMask: number[][]): number {
    const tree = Number(this.forest[row][col]);
    if (tree > smallestTree) {
      visibilityMask[row][col] = 1;
      return tree;
    }
    return smallestTree;
  }

  private getScenicScoreMask(): number[][] {
    const scenicScoreMask = this.createMaskFromForest();

    for (let row = 1; row < this.columnLength - 1; row++) {
      for (let col = 1; col < this.rowLength - 1; col++) {
        scenicScoreMask[row][col] = this.getScenicScore(row, col);
      }
    }
    
    return scenicScoreMask;
  }

  private getScenicScore(row: number, col: number): number {
    const tree = Number(this.forest[row][col]);
    let [bottom, left, right, top] = [0, 0, 0, 0];

    for (let i = row + 1; i < this.columnLength; i++) {
      bottom++;
      if (Number(this.forest[i][col]) >= tree) {
        i = this.columnLength;
      }
    }

    for (let i = row - 1; i >= 0; i--) {
      top++;
      if (Number(this.forest[i][col]) >= tree) {
        i = -1;
      }
    }

    for (let i = col + 1; i < this.rowLength; i++) {
      right++;
      if (Number(this.forest[row][i]) >= tree) {
        i = this.rowLength;
      }
    }

    for (let i = col - 1; i >= 0; i--) {
      left++;
      if (Number(this.forest[row][i]) >= tree) {
        i = -1;
      }
    }

    return bottom * left * right * top;
  }

  private createMaskFromForest(): number[][] {
    return Array
    .from({ length: this.columnLength })
    .map(() => Array
      .from({ length: this.rowLength })
      .map(() => 0)
    );
  }

  private get rowLength(): number {
    return this.forest[0].length;
  }

  private get columnLength(): number {
    return this.forest.length;
  }
}
