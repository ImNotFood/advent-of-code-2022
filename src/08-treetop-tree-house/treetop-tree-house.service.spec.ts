import { Test, TestingModule } from '@nestjs/testing';
import { TreetopTreeHouseService } from './treetop-tree-house.service';

describe('TreetopTreeHouseService', () => {
  let service: Promise<TreetopTreeHouseService>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TreetopTreeHouseService],
    }).compile();

    service = module.resolve<TreetopTreeHouseService>(TreetopTreeHouseService);
  });

  const forest = `30373\n25512\n65332\n33549\n35390`;

  it(`should count visible trees`, async () => {
    expect((await service).countVisibleTrees(forest)).toBe(21);
  });
  
  it(`should get best scenic score`, async () => {
    expect((await service).getBestScenicScore(forest)).toBe(8);
  });
});
