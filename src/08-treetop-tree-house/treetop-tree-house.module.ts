import { Module } from '@nestjs/common';
import { TreetopTreeHouseController } from './treetop-tree-house.controller';
import { TreetopTreeHouseService } from './treetop-tree-house.service';

@Module({
  controllers: [TreetopTreeHouseController],
  providers: [TreetopTreeHouseService]
})
export class TreetopTreeHouseModule {}
