import { Body, Controller, Post } from '@nestjs/common';
import { TreetopTreeHouseService } from './treetop-tree-house.service';

@Controller('treetop-tree-house')
export class TreetopTreeHouseController {
  constructor(private treetopTreeHouseService: TreetopTreeHouseService) {}

  @Post('count-visible-trees')
  countVisibleTrees(@Body('forest') forest: string): number {
    return this.treetopTreeHouseService.countVisibleTrees(forest);
  }

  @Post('best-scenic-score')
  getBestScenicScore(@Body('forest') forest: string): number {
    return this.treetopTreeHouseService.getBestScenicScore(forest);
  }
}
