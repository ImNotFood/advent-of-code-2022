import { Body, Controller, Post } from '@nestjs/common';
import { RockPaperScissorsService } from './rock-paper-scissors.service';

@Controller('rock-paper-scissors')
export class RockPaperScissorsController {
  constructor(private rockPaperScissorsService: RockPaperScissorsService) {}
  
  @Post('get-shape-score')
  getScoreFromShapes(@Body('rounds') rounds: string): number {
    return this.rockPaperScissorsService.getScoreFromShapeNames(rounds);
  }

  @Post('get-result-score')
  getScoreFromResults(@Body('rounds') rounds: string): number {
    return this.rockPaperScissorsService.getScoreFromResults(rounds);
  }
}
