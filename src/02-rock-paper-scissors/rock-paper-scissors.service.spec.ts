import { Test, TestingModule } from '@nestjs/testing';
import { RockPaperScissorsService } from './rock-paper-scissors.service';

describe('RockPaperScissorsService', () => {
  let service: RockPaperScissorsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [RockPaperScissorsService],
    }).compile();

    service = module.get<RockPaperScissorsService>(RockPaperScissorsService);
  });

  describe('getScoreFromShapeNames()', () => {
    const testCases = [
      { input: 'A X', expected: 4 },
      { input: 'A Y', expected: 8 },
      { input: 'A Z', expected: 3 },
      { input: 'B X', expected: 1 },
      { input: 'B Y', expected: 5 },
      { input: 'B Z', expected: 9 },
      { input: 'C X', expected: 7 },
      { input: 'C Y', expected: 2 },
      { input: 'C Z', expected: 6 },
      { input: 'A Y\nB X\nC Z', expected: 15 },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with input = ${data.input}`, () => {
        expect(service.getScoreFromShapeNames(data.input)).toBe(data.expected);
      });
    });
  });

  describe('getScoreFromResults()', () => {
    const testCases = [
      { input: 'A X', expected: 3 },
      { input: 'A Y', expected: 4 },
      { input: 'A Z', expected: 8 },
      { input: 'B X', expected: 1 },
      { input: 'B Y', expected: 5 },
      { input: 'B Z', expected: 9 },
      { input: 'C X', expected: 2 },
      { input: 'C Y', expected: 6 },
      { input: 'C Z', expected: 7 },
      { input: 'A Y\nB X\nC Z', expected: 12 },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with input = ${data.input}`, () => {
        expect(service.getScoreFromResults(data.input)).toBe(data.expected);
      });
    });
  });
});
