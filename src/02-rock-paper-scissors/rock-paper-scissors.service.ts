import { Injectable } from '@nestjs/common';
import { LoopedArray } from './looped-array';

export const enum Shape {
  ROCK = 0,
  PAPER = 1,
  SCISSORS = 2,
}
export enum Result {
  LOSE,
  DRAW = 3,
  WIN = 6,
}
@Injectable()
export class RockPaperScissorsService {
  private shapes = new LoopedArray<Shape>(Shape.ROCK, Shape.PAPER, Shape.SCISSORS);
  private stringToShape: Record<string, Shape> = {
    A: Shape.ROCK, B: Shape.PAPER, C: Shape.SCISSORS,
    X: Shape.ROCK, Y: Shape.PAPER, Z: Shape.SCISSORS,
  };
  private resultToScore: Record<string, Result> = {
    X: Result.LOSE, Y: Result.DRAW, Z: Result.WIN,
  };
  
  getScoreFromShapeNames(strategyGuide: string): number {
    return this
      .parseShapeInput(strategyGuide)
      .map(round => this.calculateRoundScoreFromShapeNames(round))
      .reduce((total, current) => total + current, 0);
  }

  private parseShapeInput(strategyGuide: string): [Shape, Shape][] {
    return strategyGuide
      .split('\n')
      .map(round => round.split(' '))
      .map(round => [this.stringToShape[round[0]], this.stringToShape[round[1]]]);
  }

  private calculateRoundScoreFromShapeNames([opponentShape, playerShape]: [Shape, Shape]): number {
    const shapeScore = playerShape + 1;

    if (opponentShape === playerShape) {
      return shapeScore + Result.DRAW;
    } else if (opponentShape === this.shapes.at(playerShape - 1)) {
      return shapeScore + Result.WIN;
    }
    return shapeScore + Result.LOSE;
  }

  getScoreFromResults(strategyGuide: string): number {
    return this
      .parseResultInput(strategyGuide)
      .map(round => this.calculateRoundScoreFromResults(round))
      .reduce((total, current) => total + current, 0);
  }

  private parseResultInput(strategyGuide: string): [Shape, Result][] {
    return strategyGuide
      .split('\n')
      .map(round => round.split(' '))
      .map(([shape, result]) => [this.stringToShape[shape], this.resultToScore[result]]);
  }

  private calculateRoundScoreFromResults([opponentShape, result]: [Shape, Result]): number {
    return result + this.shapes.at(opponentShape + result / 3 - 1)! + 1;
  }
}