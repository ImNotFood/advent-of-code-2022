export class LoopedArray<T> extends Array<T> {
  at(index: number): T | undefined {
    return super.at(index % this.length);
  }
}
