import { Body, Controller, Post } from '@nestjs/common';
import { CalorieCountingService } from './calorie-counting.service';

@Controller('calorie-counting')
export class CalorieCountingController {
  constructor(private calorieCountingService: CalorieCountingService) {}

  @Post('max-calories')
  getMaxCalories(@Body('calories') calories: string): number {
    return this.calorieCountingService.getMostCaloriesCarried(calories);
  }

  @Post('three-max-calories')
  getThreeMaxCalories(@Body('calories') calories: string): number {
    return this.calorieCountingService.getTotalThreeMaxCaloriesCarried(calories);
  }
}
