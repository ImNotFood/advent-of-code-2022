import { Test, TestingModule } from '@nestjs/testing';
import { CalorieCountingService } from './calorie-counting.service';

describe('CalorieCountingService', () => {
  let service: CalorieCountingService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CalorieCountingService],
    }).compile();

    service = module.get<CalorieCountingService>(CalorieCountingService);
  });

  describe('getMostCaloriesCarried()', () => {
    const testCases = [
      {
        input: '1\n2\n3\n\n4\n\n5\n6\n\n7\n8\n9\n\n10',
        expected: 24,
      },
      {
        input: '5',
        expected: 5,
      },
      {
        input: '5\n6',
        expected: 11,
      },
      {
        input: '1\n2\n\n2\n1\n\n2',
        expected: 3,
      },
      {
        input: '12\n\n1\n1\n1',
        expected: 12,
      },
      {
        input: '2\n\n1\n1\n1',
        expected: 3,
      },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with input = ${data.input}`, () => {
        expect(service.getMostCaloriesCarried(data.input)).toBe(data.expected);
      });
    });
  });

  describe('getTotalThreeMaxCaloriesCarried()', () => {
    const testCases = [
      {
        input: '1\n2\n3\n\n4\n\n5\n6\n\n7\n8\n9\n\n10',
        expected: 45,
      },
      {
        input: '5',
        expected: 5,
      },
      {
        input: '5\n\n6',
        expected: 11,
      },
      {
        input: '1\n2\n\n2\n1\n\n2',
        expected: 8,
      },
      {
        input: '12\n\n1\n1\n1\n\n1\n\n1',
        expected: 16,
      },
      {
        input: '1\n\n1\n\n1\n\n1',
        expected: 3,
      },
    ];

    testCases.forEach(data => {
      it(`should return ${data.expected} with input = ${data.input}`, () => {
        expect(service.getTotalThreeMaxCaloriesCarried(data.input)).toBe(data.expected);
      });
    });
  });
});
