import { Injectable } from '@nestjs/common';

@Injectable()
export class CalorieCountingService {
  getMostCaloriesCarried(caloriesList: string): number {
    const totalCaloriesByElf = this.parseCaloriesList(caloriesList).map(this.addCalories);
    return Math.max(...totalCaloriesByElf);
  }

  getTotalThreeMaxCaloriesCarried(caloriesList: string): number {
    const totalCaloriesByElf = this.parseCaloriesList(caloriesList).map(this.addCalories);
    return this.addCalories(this.getThreeMaxNumbers(totalCaloriesByElf));
  }

  private parseCaloriesList(caloriesList: string): number[][] {
    return caloriesList
      .split('\n\n')
      .map(caloriesByElf => caloriesByElf.split('\n'))
      .map(caloriesByElf => caloriesByElf.map(c => parseInt(c)));
  }

  private addCalories(calories: number[]): number {
    return calories.reduce((previous, current) => previous + current, 0);
  }

  private getThreeMaxNumbers(numbers: number[]): number[] {
    return numbers.sort((a, b) => b - a).slice(0, 3);
  }
}
