import { Module } from '@nestjs/common';
import { CalorieCountingService } from './calorie-counting.service';
import { CalorieCountingController } from './calorie-counting.controller';

@Module({
  providers: [CalorieCountingService],
  controllers: [CalorieCountingController],
})
export class CalorieCountingModule {}
